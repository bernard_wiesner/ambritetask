import fs from "fs";
import { sortDataByLatLong } from "./task2";
import { findClosestEntry } from "./task3";

const paths: string[] = ["./data/data.json", "./data/geo.json"];

export interface DataInterface {
  active: number;
  asn: number;
  countrycode: string;
  id: number;
  statecode: string;
  meta: string;
  distance: number;
}

export interface GeoInterface {
  ipv4: string;
  geo: string;
}

/**
 * Attempts to validate that the file at the path contains valid JSON data type
 *
 * Does a simple to check if the file on the given path can be parsed to a json
 * object. Returns true if the file can be parsed and, false if it cannot.
 * Calls validateElements with a object key validator to loop over all json objects
 * in the array and check if they contain the right keys.
 *
 * @param path The path to the file that needs to validated
 * @param validator A function that checks if a given json object is of the correct type
 */
function validateJson(path: string, validator: any): boolean {
  var data: string = fs.readFileSync(path, "utf8");
  try {
    var temp = JSON.parse(data);
    return validateElements(temp, validator);
  } catch (error) {
    return false;
  }
}

/**
 * Loops over all of the element in the given dataObject and applies the provided
 * validator to each element.
 *
 * The validator is applied to each element to verify that they conform to the
 * required structure defined by the validator.
 *
 * Returns true if all of the elements conform to the structure given by the validator
 * and false otherwise
 *
 * @param dataObject The array of objects to loop over
 * @param validator The validator to be applied to each element
 */
function validateElements(dataObject: any, validator: any) {
  for (var i = 0; i < dataObject.length; i++) {
    if (!validator(dataObject[i])) {
      console.error(
        "The following element in data.json is not a valid structure:"
      );
      console.error(dataObject[i]);
      return false;
    }
  }
  return true;
}

/**
 * Checks if a given object's keys conform to the required structure defined
 * in the dataInterface interface.
 *
 * @param object The object to validate
 */
function isDataInterfaceType(object: any): object is DataInterface {
  let b: boolean =
    "active" in object &&
    "asn" in object &&
    "countrycode" in object &&
    "id" in object &&
    "statecode" in object &&
    "meta" in object;
  return b;
}

/**
 * Checks if a given object's keys conform to the required structure defined
 * in the geoInterface interface.
 *
 * @param object The object to validate
 */
function isGeoInterfaceType(object: any): object is GeoInterface {
  let b: boolean = "ipv4" in object && "geo" in object;
  return b;
}

/**
 * Writes the given array of DataInterface array to a file called "data.json" in the "output" dir in the roor of the folder
 *
 * Checks if the 'output' dir exists: if the directory does not exist first create it
 * and then write the data to the 'data.json' file
 *
 * @param objToWrite The data to write to './output/data.json' must be of @type DataInterface[]
 */
function writeToOutputDir(objToWrite: DataInterface[]) {
  console.log("Wrote sorted rows to output/data.json");

  //Check if 'output' dir exists
  if (!fs.existsSync("output")) {
    fs.mkdirSync("output");
  }

  if (objToWrite.length === 0) {
    fs.writeFileSync("./output/data.json", "Json object was empty");
  } else {
    fs.writeFileSync("./output/data.json", JSON.stringify(objToWrite));
  }
}

///////////////////////////////////////////////////////////////////////////
//                   TASK FUNCTIONS CALLS                               //
//////////////////////////////////////////////////////////////////////////

//Kindly change the paramater values of the function calls to run with custom
//input.

//TASK 1
console.log("TASK ONE:");
var isValid1: boolean = validateJson(paths[0], isDataInterfaceType);
var isValid2: boolean = validateJson(paths[1], isGeoInterfaceType);
console.log("Data.json: " + (isValid1 ? "Is Valid" : "Is not valid"));
console.log("Geo.json: " + (isValid2 ? "Is Valid" : "Is not valid"));

//TASK 2
console.log("\nTASK TWO:");
const objToWrite = sortDataByLatLong(40.640976, -73.7919, paths); //NEW YORK
if (objToWrite != undefined) writeToOutputDir(objToWrite);

//TASK 3
console.log("\nTASK THREE: ");
console.log(findClosestEntry(-22.3, 0.7919, paths));
