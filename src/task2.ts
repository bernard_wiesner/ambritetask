import fs from "fs";
import { DataInterface, GeoInterface } from "./app";

/**
 * Entry point for the second task.
 *
 * Sorts the data.json input file according to the given latitude and
 * longitude values. Performs the sort by first calculating the distance
 * for each element of the data.
 *
 * Distance is calculated by first finding the matching the 'geo.json'
 * data with the IPv4 value. Distance is then calucalted with the
 * Haversine formula. The distance is then added to the element. After
 * all the distances have been calculated the array containing all of
 * the rows is sorted and returned
 *
 * @param lat The latitude to calculate the distances to, must be of type @type number
 * @param long The longitude to calculate the distances to, must be of @type number
 * @param paths A string array conating the path to data.json as the first element and geo.json as the second.
 */
export function sortDataByLatLong(lat: number, long: number, paths: string[]) {
  var data: string = fs.readFileSync(paths[0], "utf8");
  var geoData: string = fs.readFileSync(paths[1], "utf8");
  try {
    var dObj: DataInterface[] = JSON.parse(data);
    var gObj: GeoInterface[] = JSON.parse(geoData);
    dObj.forEach((dataElement) => {
      let ipv4 = extractIPV4FromMetaData(dataElement["meta"]);
      let geoMatch: GeoInterface = findMatchingGeoDataEntry(ipv4, gObj);
      let distance = calculateDistanceFromGeoDataElement(geoMatch, lat, long);
      dataElement.distance = distance;
    });
    dObj.sort(function (a, b) {
      var x: number = a.distance;
      var y: number = b.distance;
      return x < y ? -1 : x > y ? 1 : 0;
    });
    return dObj;
  } catch (error) {
    console.log("Not a valid json object");
    return [];
  }
}

/**
 * Extracts the IPv4 address from the given meta data string by using a regular expression
 *
 * @param metaData The meta data string containg the IPv4 address to extract
 */
function extractIPV4FromMetaData(metaData: string): string {
  var extractionRegex = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
  var r = metaData.match(extractionRegex) || "";
  return r[0];
}

/**
 * Finds the entry corresponding to the IPv4 address given in the geoData parameter
 *
 * Returns an empty GeoInterface object if no entry could found matching ipv4.
 *
 * @param ipv4 The IPv4 address of the geoData element to find
 * @param geoData The array of GeoInterface objects to search through
 */
function findMatchingGeoDataEntry(
  ipv4: string,
  geoData: GeoInterface[]
): GeoInterface {
  var ret: GeoInterface = { ipv4: "", geo: "" };

  for (var i = 0; i < geoData.length; i++) {
    let element: GeoInterface = geoData[i];

    if (element.ipv4 === ipv4) {
      return element;
    }
  }
  console.log("A corresponding geo element was not found");
  return ret;
}

/**
 * Calculates distance between sets of latitudes and longitudes using Haversine formula.
 *
 * Extracts the latitude and longitude from the geoElement object and then calculates the
 * distance in km from the geoElement to the given latitude and longitude passed as
 * parameters. Returns the distance calculated using the Haversine formula.
 *
 * @param geoElement The element containing the destincation latitude and longitude , must be of @type GeoInterface
 * @param lat The given latitude, must be of @type number
 * @param long The given longitude, must be of @type number
 */
function calculateDistanceFromGeoDataElement(
  geoElement: GeoInterface,
  lat: number,
  long: number
): number {
  var R = 6371; // Radius of the earth in km
  let destLat: number = Number.parseFloat(geoElement.geo.split(",")[0]);
  let destLong: number = Number.parseFloat(geoElement.geo.split(",")[1]);

  var dLat = deg2rad(destLat - lat);
  var dLon = deg2rad(destLong - long);

  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat)) *
      Math.cos(deg2rad(destLat)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km

  return d;
}
/**
 * Converts degrees to radians
 *
 * @param deg The degree value of @type number to convert to radians
 */
function deg2rad(deg: number): number {
  return deg * (Math.PI / 180);
}
