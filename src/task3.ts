import { sortDataByLatLong } from "./task2";
import { DataInterface } from "./app";

/**
 * Entry point for Task 3.
 *
 * Finds the entry in 'data.sjon' which is closests to the given latitude and longitude.
 * Returns a formatted string representation of the element found through the search.
 *
 * @param lat The target latitude, must be of @type number
 * @param long The target longitude, must be of @type number
 * @param paths A string array conating the path to data.json as the first element and geo.json as the second.
 */
export function findClosestEntry(
  lat: number,
  long: number,
  paths: string[]
): string {
  var sortedRows: DataInterface[] = sortDataByLatLong(lat, long, paths) || [];
  if (sortedRows.length === 0) {
    return "The sorted Rows object returned by sortDataByLatLong was empty :(";
  }
  var closestElement = sortedRows[0];
  //   console.table(closestElement);
  var s =
    "Closest Entry to (" +
    lat +
    ", " +
    long +
    "):\n" +
    "==================================================== \n" +
    "ID: " +
    closestElement.id +
    " \n" +
    "Active: " +
    (closestElement.active === 1 ? "Yes" : "No") +
    "\n" +
    "Country Code: " +
    closestElement.countrycode +
    " \n" +
    (closestElement.statecode === null
      ? ""
      : "State Code: " + closestElement.statecode + "\n") +
    "Meta Data: " +
    closestElement.meta +
    "\n" +
    "Distance Away: " +
    closestElement.distance.toFixed(2) +
    "km \n" +
    "==================================================== \n";
  return s;
}
