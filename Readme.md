# Coding task for Ambrite in TypeScript

The solution is in TypeScript and console based.

## Explanation of solution

### Task 1

For this task a simple check is first performed using JSON.parse() to determine whether or not the given input file is a valid JSON file. Next a validator is applied to each element of each respective file to verify that each json elements of each file contains the correct set of keys defined by the given structure of each respective json file's data objects.

### Task 2

To sort the data in the given 'data.json' file by 'distance to a given latitude and longitude' the IPv4 is extracted from the 'meta' field of any given json object. This IPv4 address is then used to look up the associated geo point data entry from the 'geo.json' file. The geo data (latitude and longitude) is then extracted from the geoData extracted from 'geo.json' earlier and used in the Haversine formula to calculate the distance. To stop cluttering of the console the output from this function is written to 'output/data.json' where the sorted data may be viewed.

### Task 3

This task simply calls the task 2 function, takes the top element and neatly outputs the result to standard output.

## How to run with custom values

The function calls for each task is definied at the bottom of the 'src/app.ts' file. To run with custom values, kindly change the parameter values of the functions.

## Instruction to install and run

First navigate to the root of the project and install the node packages

`npm install`

To execute the submission (again from the root of the project) run the following command

`ts-node .\src\app.ts`
